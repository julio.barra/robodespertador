#include <Wire.h>
#include <RTClib.h>   // incluye libreria para el manejo del modulo RTC

int TRIG = 13;     
int ECO = 12;      
int DURACION;
int DISTANCIA;

int IN1 = 2;      // IN1 a pin digital 2
int IN2 = 3;      // IN2 a pin digital 3
int ENA = 5;      // ENA a pin digital 5
int IN3 = 7;      // IN3 a pin digital 7
int IN4 = 8;      // IN4 a pin digital 8
int ENB = 9;      // ENA a pin digital 9

int bandera = 1;


int VELOCIDAD_A;
int VELOCIDAD_B;

#define BUZZER_ACTIVO 4 

String hora = "";
int minut = 9999;
int horaInt = 9999;

int tiempoAtrapado;

RTC_DS3231 rtc;     // crea objeto del tipo RTC_DS3231

void setup() {

  Wire.begin(8);                /* join i2c bus with address 8 */
  Wire.onReceive(receiveEvent); /* register receive event */
  Serial.begin(9600);           /* start serial for debug */

  pinMode(IN1, OUTPUT);   // IN1 como salida  
  pinMode(IN2, OUTPUT);   // IN2 como salida
  pinMode(ENA, OUTPUT);   // ENA como salida
  pinMode(IN3, OUTPUT);   // IN3 como salida
  pinMode(IN4, OUTPUT);   // IN4 como salida
  pinMode(ENB, OUTPUT);   // ENB como salida

  pinMode(11, INPUT);

  pinMode (BUZZER_ACTIVO, OUTPUT);
  
  pinMode(TRIG, OUTPUT);  // trigger como salida
  pinMode(ECO, INPUT);    // echo como entradasalida

 if (! rtc.begin()) {       // si falla la inicializacion del modulo
 Serial.println("Modulo RTC no encontrado !");  // muestra mensaje de error
 while (1);         // bucle infinito que detiene ejecucion del programa
 }
 rtc.adjust(DateTime(__DATE__, __TIME__));
}

void loop() {
  DateTime fecha = rtc.now();      // funcion que devuelve fecha y horario en formato
            // DateTime y asigna a variable fecha
   
  if(fecha.hour() == horaInt && fecha.minute() == minut && bandera == 1){
      digitalWrite(TRIG, HIGH);     // generacion del pulso a enviar
      delay(1);       // al pin conectado al trigger
      digitalWrite(TRIG, LOW);    // del sensor
      
      DURACION = pulseIn(ECO, HIGH);  // con funcion pulseIn se espera un pulso
                // alto en Echo
      DISTANCIA = DURACION / 58.2;    // distancia medida en centimetros
      delay(200);       // demora entre datos
    
      VELOCIDAD_A = 150;
      VELOCIDAD_B = 150;

      aAvance(VELOCIDAD_A);    // funcion de avance del motor A
      bAvance(VELOCIDAD_B);

      if(30 > DISTANCIA && DISTANCIA >= 0)
      {
        detieneAmbos();    // funcion que detiene ambos motores
      }
      else
      {
        aAvance(VELOCIDAD_A);    // funcion de avance del motor A
        bAvance(VELOCIDAD_B);
      }
      digitalWrite(BUZZER_ACTIVO, HIGH);  // activa buzzer

       if(digitalRead(11) == LOW)
      {
        bandera = 0;
        String asd = "lol";
        char buffer[3];
        asd.toCharArray(buffer,3);
        Wire.beginTransmission(8); /* begin with device address 8 */
         Wire.write(buffer);  /* sends hello string */
         Wire.endTransmission();
      }
      
  }else{
    digitalWrite(BUZZER_ACTIVO, LOW);
    detieneAmbos();
  }

  if(fecha.second() == 59)
  {
    bandera = 1;
  }
 delay(1000); 
 
}

// function that executes whenever data is received from master
void receiveEvent(int howMany) {
  hora = "";
 while (0 <Wire.available()) {
    char c = Wire.read();

    if(c != ':')
    {
       hora = hora + c;
    }/* print the character */
  }

  horaInt = hora.toInt();
  minut = horaInt % 100;
  horaInt -= minut;
  horaInt = horaInt/100;
    
  Serial.print(horaInt);
  Serial.print(minut);
 Serial.println();             /* to newline */
}

void aAvance(int veloc){  // funcion para avance de motor A
  analogWrite(ENA, veloc);  // velocidad mediante PWM en ENA
  digitalWrite(IN1, LOW); // IN1 a cero logico
  digitalWrite(IN2, HIGH);  // IN2 a uno logico
}

void bAvance(int veloc){  // funcion para avance de motor B
  analogWrite(ENB, veloc);  // velocidad mediante PWM en ENB
  digitalWrite(IN3, LOW); // IN3 a cero logico
  digitalWrite(IN4, HIGH);  // IN4 a uno logico
}

void detieneAmbos(){    // funcion que detiene ambos motores
  analogWrite(ENA, 0);    // deshabilita motor A
  analogWrite(ENB, 0);    // deshabilita motor B
}
